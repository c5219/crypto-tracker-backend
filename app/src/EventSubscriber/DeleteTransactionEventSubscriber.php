<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use App\Service\TransactionTotalsRecalculationService;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DeleteTransactionEventSubscriber implements EventSubscriberInterface
{
    private TransactionTotalsRecalculationService $recalculationService;
    private TokenStorageInterface $tokenStorage;


    public function __construct(TransactionTotalsRecalculationService $recalculationService,
                                TokenStorageInterface                 $tokenStorage)
    {
        $this->recalculationService = $recalculationService;
        $this->tokenStorage = $tokenStorage;
    }


    #[ArrayShape([KernelEvents::VIEW => "array"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['recalculate', EventPriorities::PRE_WRITE]
        ];
    }

    public function recalculate(ViewEvent $event)
    {
        /** @var Transaction $transaction */
        $transaction = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $allowedMethods = [Request::METHOD_DELETE];

        if (!$transaction instanceof Transaction || !in_array($method, $allowedMethods)) {
            return;
        }

        // Check if user owns the wallet
        $token = $this->tokenStorage->getToken();
        if ($token == null || $transaction->getWallet()->getOwner()->getUsername() !== $token->getUser()->getUserIdentifier()) {
            throw new AccessDeniedException('You do not own this wallet!');
        }

        $this->recalculationService->recalculate($transaction->getWallet()->getId(), $transaction, true);
    }
}