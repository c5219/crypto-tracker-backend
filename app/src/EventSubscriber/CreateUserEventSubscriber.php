<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserEventSubscriber implements EventSubscriberInterface
{

    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['userCreating', EventPriorities::PRE_WRITE]
        ];
    }

    public function userCreating(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $allowedMethods = [Request::METHOD_POST];

        if (!$user instanceof User || !in_array($method, $allowedMethods)) {
            return;
        }

        // It's a user, hash the password
        $user->setPassword(
            $this->userPasswordHasher->hashPassword($user, $user->getPassword())
        );
    }
}