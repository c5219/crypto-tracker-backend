<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use App\Service\TransactionTotalsRecalculationService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CreateTransactionEventSubscriber implements EventSubscriberInterface
{
    private TransactionRepository $transactionRepository;
    private TransactionTotalsRecalculationService $recalculationService;
    private TokenStorageInterface $tokenStorage;


    public function __construct(TransactionRepository                 $transactionRepository,
                                TransactionTotalsRecalculationService $recalculationService,
                                TokenStorageInterface                 $tokenStorage)
    {
        $this->transactionRepository = $transactionRepository;
        $this->recalculationService = $recalculationService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array[]
     */
    #[ArrayShape([KernelEvents::VIEW => "array"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['enrichTransaction', EventPriorities::PRE_WRITE]
        ];
    }

    /**
     * @throws NonUniqueResultException
     */
    public function enrichTransaction(ViewEvent $event): void
    {
        /** @var Transaction $transaction */
        $transaction = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $allowedMethods = [Request::METHOD_POST];

        if (!$transaction instanceof Transaction || !in_array($method, $allowedMethods)) {
            return;
        }

        // Check if user owns the wallet
        $token = $this->tokenStorage->getToken();
        if ($token == null || $transaction->getWallet()->getOwner()->getUsername() !== $token->getUser()->getUserIdentifier()) {
            throw new AccessDeniedException('You do not own this wallet!');
        }

        $prevTotalCoinAmount = 0;
        $prevTotalFiatInvested = 0;
        try {
            $lastTransaction = $this->transactionRepository->findLastTransactionByWalletId(
                $transaction->getWallet()->getId()
            );

            $prevTotalCoinAmount = $lastTransaction->getTotalCoinAmount();
            $prevTotalFiatInvested = $lastTransaction->getTotalFiatInvested();
        } catch (NoResultException $e) {
        }


        $transaction
            ->setTotalCoinAmount($prevTotalCoinAmount + $transaction->getCoinAmount())
            ->setTotalFiatInvested($prevTotalFiatInvested + $transaction->getFiatInvested());

        $this->recalculationService->recalculate($transaction->getWallet()->getId(), $transaction);
    }
}