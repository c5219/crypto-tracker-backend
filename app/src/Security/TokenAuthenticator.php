<?php

namespace App\Security;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\JWTAuthenticator;
use Symfony\Component\Security\Core\User\UserInterface;

class TokenAuthenticator extends JWTAuthenticator
{

    protected function loadUser(array $payload, string $identity): UserInterface
    {
        /** @var User $user */
        $user = parent::loadUser($payload, $identity);

        if ($user->getPasswordChangeDate() && $payload['iat'] < $user->getPasswordChangeDate()) {
            throw new ExpiredTokenException();
        }

        return $user;
    }
}
