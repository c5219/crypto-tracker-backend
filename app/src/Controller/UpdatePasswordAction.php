<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\DTO\UserPasswordUpdateDTO;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class UpdatePasswordAction extends AbstractController
{

    private ValidatorInterface $validator;
    private UserPasswordHasherInterface $userPasswordHasher;
    private EntityManagerInterface $entityManager;
    private JWTTokenManagerInterface $tokenManager;
    private UserRepository $userRepository;

    /**
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $entityManager
     * @param JWTTokenManagerInterface $tokenManager
     * @param UserRepository $userRepository
     */
    public function __construct(ValidatorInterface $validator, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, JWTTokenManagerInterface $tokenManager, \App\Repository\UserRepository $userRepository)
    {
        $this->validator = $validator;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->entityManager = $entityManager;
        $this->tokenManager = $tokenManager;
        $this->userRepository = $userRepository;
    }

    #[Route(path: '/api/update_password', methods: ['POST'])]
    public function updatePassword(Request $request): JsonResponse
    {
        $raw = json_decode($request->getContent(), true);

        $dto = new UserPasswordUpdateDTO();
        $dto->setUsername($raw["username"])
            ->setCurrentPassword($raw["currentPassword"])
            ->setNewPassword($raw["newPassword"]);


        $user = $this->userRepository->findOneBy(["username" => $dto->getUsername()]);

        if (!$user) {
            throw new \InvalidArgumentException("Username or current password is invalid");
        }

        $isValid = $this->userPasswordHasher->isPasswordValid($user, $dto->getCurrentPassword());

        if (!$isValid) {
            throw new \InvalidArgumentException("Username or current password is invalid");
        }

        $hashedPassword = $this->userPasswordHasher->hashPassword($user, $dto->getNewPassword());
        $this->userRepository->changePassword($user, $hashedPassword);

        return new JsonResponse(['token' => $this->tokenManager->create($user)]);
    }
}
