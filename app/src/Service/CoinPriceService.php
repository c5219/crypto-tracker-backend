<?php

namespace App\Service;

use Scheb\YahooFinanceApi;


class CoinPriceService
{

    private YahooFinanceApi\ApiClient $client;

    public function __construct()
    {
        $this->client = YahooFinanceApi\ApiClientFactory::createApiClient();
    }

    public function fetchValue(string $ticker, string $currency): float
    {
        $quote = $this->client->getQuote("$ticker-$currency");
        return $quote?->getRegularMarketPrice();
    }
}