<?php

namespace App\Service;

use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Validator\Exception\BadMethodCallException;

class TransactionTotalsRecalculationService
{

    private EntityManagerInterface $entityManager;
    private TransactionRepository $transactionRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->transactionRepository = $entityManager->getRepository(Transaction::class);
    }

    public function recalculate(int $walletId, ?Transaction $transactionInEdition, bool $isDelete = false)
    {
        if (isset($transactionInEdition) && $walletId !== $transactionInEdition->getWallet()->getId()) {
            throw new BadMethodCallException("Wallet mismatch between walletId and startingTransaction");
        }

        if (isset($transactionInEdition)) {
            $transactions = $this->transactionRepository->findByWalletIdAndDateTimeAfter(
                $walletId, $transactionInEdition->getDatetime(), 'ASC'
            );
        } else {
            $transactions = $this->transactionRepository->findByWalletId($walletId, 'ASC');
        }


        if (count($transactions) === 0) {
            # there isn't any newer transaction
            return;
        }

        // Get to the state before the last transaction
        $totalFiatSum = $transactions[0]->getTotalFiatInvested() - $transactions[0]->getFiatInvested();
        $totalCoinSum = $transactions[0]->getTotalCoinAmount() - $transactions[0]->getCoinAmount();

        // If we are putting a transaction into the middle, set the value for the new transaction being inserted
        // If we are deleting a transaction, subtract the values
        if (isset($transactionInEdition)) {
            if ($isDelete) {
                $totalFiatSum = bcsub($totalFiatSum, $transactionInEdition->getFiatInvested(), 4);
                $totalCoinSum = bcsub($totalCoinSum, $transactionInEdition->getCoinAmount(), 10);
            } else {
                $totalFiatSum = bcadd($totalFiatSum, $transactionInEdition->getFiatInvested(), 4);
                $totalCoinSum = bcadd($totalCoinSum, $transactionInEdition->getCoinAmount(), 10);

                $transactionInEdition->setTotalFiatInvested($totalFiatSum);
                $transactionInEdition->setTotalCoinAmount($totalCoinSum);
            }
        }

        // Increment the totals of the next transactions
        foreach ($transactions as $transaction) {
            if ($isDelete && $transaction === $transactionInEdition) {
                // Ignore transaction being deleted
                continue;
            }

            $totalFiatSum = bcadd($totalFiatSum, $transaction->getFiatInvested(), 4);
            $totalCoinSum = bcadd($totalCoinSum, $transaction->getCoinAmount(), 10);

            $transaction->setTotalFiatInvested($totalFiatSum);
            $transaction->setTotalCoinAmount($totalCoinSum);
        }

        $this->entityManager->flush();
    }
}