<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;
use ArrayObject;

class UpdatePasswordDecorator implements OpenApiFactoryInterface
{
    private OpenApiFactoryInterface $decorated;

    public function __construct(
        OpenApiFactoryInterface $decorated
    )
    {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['UpdatePassword'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'johndoe',
                ],
                'currentPassword' => [
                    'type' => 'string',
                    'example' => 'password'
                ],
                'newPassword' => [
                    'type' => 'string',
                    'example' => 'newPassword'
                ]
            ],
        ]);

        $pathItem = new Model\PathItem(
            post: new Model\Operation(
                operationId: 'updatePassword',
                tags: ['Password'],
                responses: [
                    '200' => [
                        'description' => 'New JWT Token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token'
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Updates the user password.',
                requestBody: new Model\RequestBody(
                    description: 'Change password',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/UpdatePassword'
                            ],
                        ],
                    ])
                )
            )
        );

        $openApi->getPaths()->addPath('/api/update_password', $pathItem);

        return $openApi;
    }
}