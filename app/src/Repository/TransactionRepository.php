<?php

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @param int $walletId
     * @return Transaction[]
     */
    public function findByWalletId(int $walletId, string $sortDir = 'DESC'): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.wallet', 'w')
            ->andWhere('w.id = :walletId')
            ->setParameter('walletId', $walletId)
            ->addOrderBy('t.datetime', $sortDir)
            ->addOrderBy('t.id', $sortDir)
            ->getQuery()
            ->getResult();
    }

    public function findByWalletIdAndDateTimeAfter(int $walletId, \DateTimeInterface $startDateTime, $sortDir = 'DESC'): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.wallet', 'w')
            ->andWhere('w.id = :walletId')
            ->andWhere('t.datetime > :startDateTime')
            ->setParameter('walletId', $walletId)
            ->setParameter('startDateTime', $startDateTime)
            ->addOrderBy('t.datetime', $sortDir)
            ->addOrderBy('t.id', $sortDir)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $walletId
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @return Transaction
     */
    public function findLastTransactionByWalletId(int $walletId): Transaction
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.wallet', 'w')
            ->andWhere('w.id = :walletId')
            ->setParameter('walletId', $walletId)
            ->addOrderBy('t.datetime', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    // /**
    //  * @return Transaction[] Returns an array of Transaction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transaction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
