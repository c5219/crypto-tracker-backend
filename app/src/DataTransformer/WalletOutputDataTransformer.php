<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Entity\DTO\WalletOutputDTO;
use App\Entity\Transaction;
use App\Entity\Wallet;
use App\Repository\TransactionRepository;
use App\Service\CoinPriceService;
use Doctrine\ORM\NoResultException;

class WalletOutputDataTransformer implements DataTransformerInterface
{

    private TransactionRepository $transactionRepository;
    private CoinPriceService $coinPriceService;

    /**
     * @param TransactionRepository $transactionRepository
     * @param CoinPriceService $coinPriceService
     */
    public function __construct(TransactionRepository $transactionRepository,
                                CoinPriceService      $coinPriceService)
    {
        $this->transactionRepository = $transactionRepository;
        $this->coinPriceService = $coinPriceService;
    }


    /**
     * {@inheritDoc}
     *
     * @param $object Wallet
     * @return WalletOutputDTO
     */
    public function transform($object, string $to, array $context = []): WalletOutputDTO
    {
        $coinValue = $this->coinPriceService->fetchValue($object->getCoin(), $object->getFiatCurrency());
        $lastTransaction = $this->fetchLastTransaction($object->getId());

        $out = new WalletOutputDTO();
        $out->setId($object->getId())
            ->setName($object->getName())
            ->setCoin($object->getCoin())
            ->setFiatCurrency($object->getFiatCurrency())
            ->setCoinValue($coinValue)
            ->setTotalCoinAmount($lastTransaction?->getTotalCoinAmount() ?? 0)
            ->setTotalFiatInvested($lastTransaction?->getTotalFiatInvested() ?? 0)
            ->setFiatWalletValue(bcmul($coinValue, $out->getTotalCoinAmount(), 4))
            ->setRoi(bcsub($out->getFiatWalletValue(), $out?->getTotalFiatInvested()))
            ->setRoiPercent($out->getTotalFiatInvested() == 0 ? 100 :
                bcmul(bcdiv($out->getRoi(), $out->getTotalFiatInvested(), 4), 100, 4)
            )
            ->setBreakEvenCoinValue(
                $out->getFiatWalletValue() == 0 ? 0 :
                    bcdiv(
                        bcmul($out?->getCoinValue(), $out->getTotalFiatInvested(), 10),
                        $out->getFiatWalletValue(),10
                    )
            );

        return $out;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return WalletOutputDTO::class === $to && $data instanceof Wallet;
    }

    private function fetchLastTransaction(int $walletId): ?Transaction
    {
        try {
            return $this->transactionRepository->findLastTransactionByWalletId($walletId);
        } catch (NoResultException | \Exception $e) {
        }

        return null;
    }
}