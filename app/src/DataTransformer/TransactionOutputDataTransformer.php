<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Entity\DTO\TransactionOutputDTO;
use App\Entity\Transaction;

class TransactionOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritDoc}
     *
     * @param $object Transaction
     * @return TransactionOutputDTO
     */
    public function transform($object, string $to, array $context = []): TransactionOutputDTO
    {
        $out = new TransactionOutputDTO();
        $out->setId($object->getId())
            ->setWallet($object->getWallet()->getId())
            ->setDateTime($object->getDatetime())
            ->setCoinValue($object->getCoinValue())
            ->setCoinValue($object->getCoinValue())
            ->setCoinAmount($object->getCoinAmount())
            ->setFiatInvested($object->getFiatInvested())
            ->setTotalCoinAmount($object->getTotalCoinAmount())
            ->setTotalFiatInvested($object->getTotalFiatInvested())
            ->setFiatWalletValue($object->getCoinValue() * $object->getTotalCoinAmount())
            ->setFiatROI($out->getFiatWalletValue() - $object->getTotalFiatInvested());

        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TransactionOutputDTO::class === $to && $data instanceof Transaction;
    }
}