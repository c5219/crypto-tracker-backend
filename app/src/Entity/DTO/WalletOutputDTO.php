<?php

namespace App\Entity\DTO;

class WalletOutputDTO
{

    private int $id;
    private string $name;
    private string $coin;
    private string $fiatCurrency;
    private ?string $coinValue;
    private string $totalFiatInvested;
    private string $totalCoinAmount;
    private string $fiatWalletValue;
    private string $roi;
    private string $roiPercent;
    private string $breakEvenCoinValue;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return WalletOutputDTO
     */
    public function setId(int $id): WalletOutputDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return WalletOutputDTO
     */
    public function setName(string $name): WalletOutputDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoin(): string
    {
        return $this->coin;
    }

    /**
     * @param string $coin
     * @return WalletOutputDTO
     */
    public function setCoin(string $coin): WalletOutputDTO
    {
        $this->coin = $coin;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiatCurrency(): string
    {
        return $this->fiatCurrency;
    }

    /**
     * @param string $fiatCurrency
     * @return WalletOutputDTO
     */
    public function setFiatCurrency(string $fiatCurrency): WalletOutputDTO
    {
        $this->fiatCurrency = $fiatCurrency;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCoinValue(): ?string
    {
        return $this->coinValue;
    }

    /**
     * @param string|null $coinValue
     * @return WalletOutputDTO
     */
    public function setCoinValue(?string $coinValue): WalletOutputDTO
    {
        $this->coinValue = $coinValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotalFiatInvested(): string
    {
        return $this->totalFiatInvested;
    }

    /**
     * @param string $totalFiatInvested
     * @return WalletOutputDTO
     */
    public function setTotalFiatInvested(string $totalFiatInvested): WalletOutputDTO
    {
        $this->totalFiatInvested = $totalFiatInvested;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotalCoinAmount(): string
    {
        return $this->totalCoinAmount;
    }

    /**
     * @param string $totalCoinAmount
     * @return WalletOutputDTO
     */
    public function setTotalCoinAmount(string $totalCoinAmount): WalletOutputDTO
    {
        $this->totalCoinAmount = $totalCoinAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiatWalletValue(): string
    {
        return $this->fiatWalletValue;
    }

    /**
     * @param string $fiatWalletValue
     * @return WalletOutputDTO
     */
    public function setFiatWalletValue(string $fiatWalletValue): WalletOutputDTO
    {
        $this->fiatWalletValue = $fiatWalletValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoi(): string
    {
        return $this->roi;
    }

    /**
     * @param string $roi
     * @return WalletOutputDTO
     */
    public function setRoi(string $roi): WalletOutputDTO
    {
        $this->roi = $roi;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoiPercent(): string
    {
        return $this->roiPercent;
    }

    /**
     * @param string $roiPercent
     * @return WalletOutputDTO
     */
    public function setRoiPercent(string $roiPercent): WalletOutputDTO
    {
        $this->roiPercent = $roiPercent;
        return $this;
    }

    /**
     * @return string
     */
    public function getBreakEvenCoinValue(): string
    {
        return $this->breakEvenCoinValue;
    }

    /**
     * @param string $breakEvenCoinValue
     * @return WalletOutputDTO
     */
    public function setBreakEvenCoinValue(string $breakEvenCoinValue): WalletOutputDTO
    {
        $this->breakEvenCoinValue = $breakEvenCoinValue;
        return $this;
    }
}