<?php

namespace App\Entity\DTO;

class UserPasswordUpdateDTO
{
    private string $username;
    private string $currentPassword;
    private string $newPassword;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserPasswordUpdateDTO
     */
    public function setUsername(string $username): UserPasswordUpdateDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentPassword(): string
    {
        return $this->currentPassword;
    }

    /**
     * @param string $currentPassword
     * @return UserPasswordUpdateDTO
     */
    public function setCurrentPassword(string $currentPassword): UserPasswordUpdateDTO
    {
        $this->currentPassword = $currentPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return UserPasswordUpdateDTO
     */
    public function setNewPassword(string $newPassword): UserPasswordUpdateDTO
    {
        $this->newPassword = $newPassword;
        return $this;
    }
}
