<?php

namespace App\Entity\DTO;

class TransactionOutputDTO
{

    private int $id;
    private string $wallet;
    private \DateTimeInterface $dateTime;
    private string $coinValue;
    private string $coinAmount;
    private string $fiatInvested;
    private string $totalCoinAmount;
    private string $totalFiatInvested;

    private string $fiatWalletValue;
    private string $fiatROI;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TransactionOutputDTO
     */
    public function setId(int $id): TransactionOutputDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getWallet(): string
    {
        return $this->wallet;
    }

    /**
     * @param string $wallet
     * @return TransactionOutputDTO
     */
    public function setWallet(string $wallet): TransactionOutputDTO
    {
        $this->wallet = $wallet;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateTime(): \DateTimeInterface
    {
        return $this->dateTime;
    }

    /**
     * @param \DateTime $dateTime
     * @return TransactionOutputDTO
     */
    public function setDateTime(\DateTimeInterface $dateTime): TransactionOutputDTO
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoinValue(): string
    {
        return $this->coinValue;
    }

    /**
     * @param string $coinValue
     * @return TransactionOutputDTO
     */
    public function setCoinValue(string $coinValue): TransactionOutputDTO
    {
        $this->coinValue = $coinValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoinAmount(): string
    {
        return $this->coinAmount;
    }

    /**
     * @param string $coinAmount
     * @return TransactionOutputDTO
     */
    public function setCoinAmount(string $coinAmount): TransactionOutputDTO
    {
        $this->coinAmount = $coinAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiatInvested(): string
    {
        return $this->fiatInvested;
    }

    /**
     * @param string $fiatInvested
     * @return TransactionOutputDTO
     */
    public function setFiatInvested(string $fiatInvested): TransactionOutputDTO
    {
        $this->fiatInvested = $fiatInvested;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotalCoinAmount(): string
    {
        return $this->totalCoinAmount;
    }

    /**
     * @param string $totalCoinAmount
     * @return TransactionOutputDTO
     */
    public function setTotalCoinAmount(string $totalCoinAmount): TransactionOutputDTO
    {
        $this->totalCoinAmount = $totalCoinAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiatWalletValue(): string
    {
        return $this->fiatWalletValue;
    }

    /**
     * @param string $fiatWalletValue
     * @return TransactionOutputDTO
     */
    public function setFiatWalletValue(string $fiatWalletValue): TransactionOutputDTO
    {
        $this->fiatWalletValue = $fiatWalletValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getTotalFiatInvested(): string
    {
        return $this->totalFiatInvested;
    }

    /**
     * @param string $totalFiatInvested
     * @return TransactionOutputDTO
     */
    public function setTotalFiatInvested(string $totalFiatInvested): TransactionOutputDTO
    {
        $this->totalFiatInvested = $totalFiatInvested;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiatROI(): string
    {
        return $this->fiatROI;
    }

    /**
     * @param string $fiatROI
     * @return TransactionOutputDTO
     */
    public function setFiatROI(string $fiatROI): TransactionOutputDTO
    {
        $this->fiatROI = $fiatROI;
        return $this;
    }
}