<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\DTO\TransactionOutputDTO;
use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    itemOperations: ["get", "delete"],
    order: ["datetime"=>"DESC"],
    output: TransactionOutputDTO::class
)]
#[ORM\UniqueConstraint(name: "wallet_datetime", columns: ["wallet_id", "datetime"])]
#[ORM\Entity(repositoryClass: TransactionRepository::class)]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Wallet::class, inversedBy: 'transactions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Wallet $wallet;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $datetime;

    #[ORM\Column(type: 'decimal', precision: 17, scale: 10)]
    private ?string $coinValue;

    #[ORM\Column(type: 'decimal', precision: 17, scale: 10)]
    private ?string $coinAmount;

    #[ORM\Column(type: 'decimal', precision: 15, scale: 4)]
    private ?string $fiatInvested;

    #[ORM\Column(type: 'decimal', precision: 17, scale: 10)]
    private ?string $totalCoinAmount;

    #[ORM\Column(type: 'decimal', precision: 17, scale: 4)]
    private ?string $totalFiatInvested;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function setWallet(?Wallet $wallet): self
    {
        $this->wallet = $wallet;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getCoinValue(): ?string
    {
        return $this->coinValue;
    }

    public function setCoinValue(string $coinValue): self
    {
        $this->coinValue = $coinValue;

        return $this;
    }

    public function getCoinAmount(): ?string
    {
        return $this->coinAmount;
    }

    public function setCoinAmount(string $coinAmount): self
    {
        $this->coinAmount = $coinAmount;

        return $this;
    }

    public function getFiatInvested(): ?string
    {
        return $this->fiatInvested;
    }

    public function setFiatInvested(string $fiatInvested): self
    {
        $this->fiatInvested = $fiatInvested;

        return $this;
    }

    public function getTotalCoinAmount(): ?string
    {
        return $this->totalCoinAmount;
    }

    public function setTotalCoinAmount(string $totalCoinAmount): self
    {
        $this->totalCoinAmount = $totalCoinAmount;

        return $this;
    }

    public function getTotalFiatInvested(): ?string
    {
        return $this->totalFiatInvested;
    }

    public function setTotalFiatInvested(string $totalFiatInvested): self
    {
        $this->totalFiatInvested = $totalFiatInvested;

        return $this;
    }
}
