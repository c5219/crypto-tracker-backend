<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\DTO\WalletOutputDTO;
use App\Repository\WalletRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    itemOperations: [
        'get',
        'put' => [
            "security" => "is_granted('IS_AUTHENTICATED_FULLY') and object.getOwner().getUsername() == user.getUsername()",
        ],
        'delete'
    ],
    denormalizationContext: ["groups" => ["wallet", "wallet:write"]],
    output: WalletOutputDTO::class
)]
#[ORM\Entity(repositoryClass: WalletRepository::class)]
class Wallet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(["wallet:write"])]
    #[ORM\Column(type: 'string', length: 100)]
    private ?string $name;

    #[Groups(["wallet:write"])]
    #[ORM\Column(type: 'string', length: 10)]
    private ?string $coin;

    #[Groups(["wallet:write"])]
    #[ORM\Column(type: 'string', length: 10)]
    private ?string $fiatCurrency;

    #[ApiSubresource]
    #[ORM\OneToMany(mappedBy: 'wallet', targetEntity: Transaction::class, orphanRemoval: true)]
    private Collection $transactions;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'wallets')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $owner;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCoin(): ?string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFiatCurrency(): ?string
    {
        return $this->fiatCurrency;
    }

    /**
     * @param string|null $fiatCurrency
     * @return Wallet
     */
    public function setFiatCurrency(?string $fiatCurrency): Wallet
    {
        $this->fiatCurrency = $fiatCurrency;
        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setWallet($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getWallet() === $this) {
                $transaction->setWallet(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
