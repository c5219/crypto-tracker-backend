#!/bin/sh

cd /app
make database-create
make migrate

php-fpm -D
nginx -g 'daemon off;'