.PHONY=docker-build
docker-build:
	docker build -t registry.gitlab.com/c5219/crypto-tracker-backend:${VERSION} -f ./docker/api/Dockerfile .

.PHONY=docker-push
docker-push:
	docker push registry.gitlab.com/c5219/crypto-tracker-backend:${VERSION}